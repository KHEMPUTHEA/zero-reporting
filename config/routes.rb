Reportwheel::Application.routes.draw do
  


  # get "tests/index"
  scope "(:locale)", locale: /en|kh/ do

    get "test/index"

    # get "ods/index"

    get "contacts/index" 
    get "places/search_place_name"
    post "contacts/create_contact"
    post "places/create_place"
    get "places/update_place"
    get "places/get_parent_level"
    get "places/get_sibling_level"

    delete "places/delete_place"
    delete "contacts/delete_contact"
    # post "users/update_phone"


    # get "reports/render_date"
    # get "reports/render_date_next_previous"
    get "reports/create_hc_chart"
    get "reports/hc_chart"
    get "reports/disease_chart"
    get "reports/create_disease_chart"
    
    resources :places do
      resources :contacts
      get 'get_children', on: :member
      
      # get 'search_place_name', on: :member
    end   

    resources :reports do
      collection do
        get :export_csv
      end
    end

    resources :users do
      get :view
      put :update_current_user
      collection do
        get :reset_password
        put :change_password          
      end
    end

  end






  
  get "logout" => "sessions#destroy", :as => "logout"
  get "login" => "sessions#new", :as => "login"
  root :to => "reports#index"

  resource :sessions
  
end