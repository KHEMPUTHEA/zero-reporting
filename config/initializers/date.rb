class Date
  def to_range type
    if type === "Weekly"
      Week.new(self)
    elsif type === "Monthly"
      Month.new(self)
    elsif type === "Yearly"
      Year.new(self)
    end
  end



end