namespace :db do 
	desc "Import data from csv file"
	task :data_migration => :environment do
		# Rake::Task["db:reset"].invoke
	  Old::Place.includes(:place_type).all.each do |place|
	  	Place.import_place place
	  end
	end

end