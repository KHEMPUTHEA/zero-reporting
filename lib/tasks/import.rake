require 'csv'

desc "Import data from csv file"
task :import => [:environment] do

  file = "tmp/csv_db/keansvayod.csv"

  CSV.foreach(file, :headers => true) do |row|
#    p "======================= #{row} ==============================="
    Report.create ({
      :sender => (del row[7]),
      :disease => (del row[2]),
      :quantity => (del row[6]).to_i,
      :from => (del row[3]),
      :lat => (del row[4]),
      :lon => (del row[5]),
      :to => (del row[8]),
      :created_at => row[0]
    })
  end
end




# delete " from string
def del str
	(str.delete("\"")).delete(" ")
end