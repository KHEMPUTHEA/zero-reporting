class RenameColumn < ActiveRecord::Migration
  def up
  	rename_column :places, :p_type, :type
  end

  # def down
  # 	rename_column :places, :p_type, :type
  # end
end
