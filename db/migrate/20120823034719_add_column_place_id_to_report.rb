class AddColumnPlaceIdToReport < ActiveRecord::Migration
  def change
    add_column :reports, :place_id, :integer, :default => nil
  end
end
