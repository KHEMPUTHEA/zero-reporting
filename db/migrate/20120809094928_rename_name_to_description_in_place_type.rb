class RenameNameToDescriptionInPlaceType < ActiveRecord::Migration
  def up
  	rename_column :place_types, :name, :description
  end

  def down
  	rename_column :place_types, :description, :name
  end
end
