class AddPlaceToUser < ActiveRecord::Migration
	def up
		change_table :users do |t|
    		t.integer :place_id, :default => nil
      end
    end

    def down
	    remove_column :users, :place_id
  	end
end
