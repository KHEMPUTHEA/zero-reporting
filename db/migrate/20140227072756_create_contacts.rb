class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :telephone_number
      t.integer :place_id

      t.timestamps
    end
  end
end
