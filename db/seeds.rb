# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# phd = PlaceType.create(code: "PHD", description: "Provincial")
# od = PlaceType.create(code: "OD", description: "Operational District")
# hc = PlaceType.create(code: "HC", description: "Health Center")

# Phnom Penh
# pp = Place.create(name: "Phnom Penh", place_type_id: phd.id)
# pp = Phd.create(id: "1", name: "Phnom Penh")
# Lavea Em OD
# levea_em_od = Place.create(name: "Lavea Em OD", place_type_id: od.id, parent_id: pp.id)
# levea_em_od = Od.create(name: "Lavea Em OD", parent_id: pp.id)




# @a = Hc.create(name: "Areiy Ksat Health Center", parent_id: levea_em_od.id)
# Hc.create(name: "Barong Health Center", parent_id: levea_em_od.id)
# @h = Hc.create(name: "Koh Keo Health Center", parent_id: levea_em_od.id)


# # Kean Svay OD
# # kean_svay_od = Place.create(name: "Kean Svay OD", place_type_id: od.id, parent_id: pp.id)

# kean_svay_od = Od.create(name: "Kean Svay OD", parent_id: pp.id)




# Hc.create(name: "Banteay Dek Health Center", parent_id: kean_svay_od.id)
# Hc.create(name: "Dei Eth Health Center", parent_id: kean_svay_od.id)
# Hc.create(name: "K'am Samnar Health Center", parent_id: kean_svay_od.id)
# Hc.create(name: "Kampong Phnom Health Center", parent_id: kean_svay_od.id)

# # admin user
User.create!(name: "mou",password_confirmation: "123456", password: "123456", can_login: true, level: 1)
# User.create!(name: "thyda",password_confirmation: "123", password: "123", place_id: 3, can_login: true, phone_number: "85510416844")
# User.create!(name: "instedd",password_confirmation: "123", password: "123", place_id: 3, can_login: true, phone_number: "85510416841")


# Report.create(:user_id => 2, :sender => "thyda", :disease => "AWD", :quantity => 10, :place_id => @a.id, :from => 'sms://85510416844', :lat => '48.74102610',
#          :lon => '-117.41718860', :to => 'sms://2020')
# Report.create(:user_id => 2, :sender => "thyda", :disease => "AFP", :quantity => 10, :place_id => @h.id, :from => 'sms://85510416844', :lat => '48.74102610',
#          :lon => '-117.41718860', :to => 'sms://2020')

