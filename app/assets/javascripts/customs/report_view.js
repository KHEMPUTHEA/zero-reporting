report_by = "Weekly";


$(document).ready(function(){



	$(document.body).on("click",".places", function(event){
    	report_request();
	});

	$(document.body).on("click","#report_by_weekly", function(event){
		event.preventDefault();
		report_by = "Weekly";
		report_request();
	});



	$(document.body).on("click","#report_by_monthly", function(event){
		event.preventDefault();
		report_by = "Monthly";
		report_request();
	});

	$(document.body).on("click","#report_by_yearly", function(event){
		event.preventDefault();
		report_by = "Yearly";
		report_request();
	});



	$(document.body).on("click","#previous_date_btn", function(event){
		event.preventDefault();
		from_date = $("#from").val();
		report_by = $("#report_by").val();

	    if (report_by == "Yearly"){
			start_date = new Date(from_date).addYears(-1) ;
	    	end_date = new Date(start_date).moveToMonth(11).moveToLastDayOfMonth()  ;
	    }else if (report_by === "Monthly"){
	    	start_date = new Date(from_date).moveToFirstDayOfMonth().addMonths(-1);//.month();
	    	end_date = new Date(start_date).moveToLastDayOfMonth();//.month();
	    }else{
	    	start_date = new Date(from_date).last().wednesday();
	    	end_date = new Date(start_date).next().tuesday();
	    }

	    start_date = start_date.toString('yyyy-M-d');
	    end_date = end_date.toString('yyyy-M-d');

	    $("#from").val(start_date);
	    $("#to").val(end_date);
	   
	    report_request();
	});


	$(document.body).on("click","#next_date_btn", function(event){
		event.preventDefault();
		from_date = $("#from").val();
		report_by = $("#report_by").val();

	    if (report_by == "Yearly"){
			start_date = new Date(from_date).addYears(1);
	    	end_date = new Date(start_date).moveToMonth(11).moveToLastDayOfMonth() ;
	    }else if (report_by === "Monthly"){
	    	start_date = new Date(from_date).moveToFirstDayOfMonth().addMonths(1);//.month();
	    	end_date = new Date(start_date).moveToLastDayOfMonth();//.month();
	    }else{
	    	start_date = new Date(from_date).next().wednesday();
	    	end_date = new Date(start_date).next().tuesday();
	    }

	 	start_date = start_date.toString('yyyy-M-d');
	    end_date = end_date.toString('yyyy-M-d');

	    $("#from").val(start_date);
	    $("#to").val(end_date);		
		
		report_request();
	});

});



function report_request(){
	from = $("#from").val();

	places = [0];
	$(".places:checked").each(function() {
        places.push($(this).val());
    });

	$.ajax({		
		type: 'GET',
		dataType: 'JSON',
	    data: {report_by:report_by, from:from, places:places},
	    success: function(response) { 
	    	window.dataSource = response.data;
	    	window.places_name = response.places;
	    	window.time_range = response.time_range;
	    	$("#report_table").html(response.html);
	    	drawChart(response.data);
	    }, 
	    error: function(error){
	    	// console.log("Error "+error);
	    }
	});
	
}















