	var parent_id ;
	var phd_id;
	var od_id;
  var current_place ; 
  
$(document).ready(function(){
  window.current_user_place_type = $('#current_user_place_type').val();
  window.current_user_place_id = $('#current_user_place_id').val();
  if(window.current_user_place_type == '')
    $('#btn_add_phd').show();
  // if(window.current_user_type == 'Phd'){
  //   $('#btn_add_od').show();
  //   $('#btn_add_phd').hide();
  //   $('#btn_add_hc').hide();
  // }else if(window.current_user_type == 'Od'){
  //   alert("OD");
  //   $('#btn_add_hc').show();
  //   $('#btn_add_od').hide();
  //   $('#btn_add_phd').hide();
  // }else{
  //   $('#btn_add_phd').show();
  // }

  $(document.body).on("click",".list1", function(event){
    // console.log("list 1");
    id = $(this).attr('data-id');
    name = $(this).attr('data-name');
    set_place_phd(id,name);
  });

  $(document.body).on("click",".list2", function(event){
    // console.log("list 2");
    id = $(this).attr('data-id');
    name = $(this).attr('data-name');
    set_place_od(id,name);
  });

  $(document.body).on("click",".list3", function(event){
    // console.log("list 3");
    id = $(this).attr('data-id');
    name = $(this).attr('data-name');
    show_contact(id,name);
  });

	$(document.body).on("keypup","#txt_search", function(event){    	
      if(event.keyCode == 13) {
      	val = $('#txt_search').val();
      	if(val != ""){
      		
			$.ajax('/places/search_place_name', {
			    type: 'GET',
			    data: { name: val },
			    success: function(response) { 
            // console.log(response);

			    			    	
  					$.each(response, function(key,value) {
              $('li').removeClass('active');
              $("#"+value['id']).parents('ul').addClass('collapse in');
              $("#"+value['id']).parents('li').addClass('active');
              // $("#"+value['id']).addClass('active');
              if(value['type'] == 'Hc'){
                show_contact(value['id'], value['name']);
              } 						
  					});

			    },
			    error: function() { 

			    }
			});
		}
      }
	});


  $(document.body).on("click",".phd_list > a", function(event){
    show_btn_od(); 
  });

  $(document.body).on("click",".od_list", function(event){
    show_btn_hc();
  });  

	$('#btn_add_phd').click(function() {
		set_state('Phd','', 'PHD');
	});

	$('#btn_add_od').click(function() {
		set_state('Od',phd_id, 'OD');
	});

	$('#btn_add_hc').click(function() {
    set_state('Hc',od_id, 'HC');
	});

   $("#btn_submit_phone").on("click",function(event) {
      tel = $('#telephone_number').val();
      $('#messge_error_telephone_number ul').empty();
      $.ajax({
        type: "POST",
        url:'contacts/create_contact', 
        data: {telephone_number: tel, place_id: current_place},
        success: function(response){    
          if(jQuery.isArray(response)){
              tmp = '';
              $.each( response, function( key, data ) {
                $('#messge_error_telephone_number ul').append("<li>"+data+"</li>");    
              });
            
          }else{
            // console.log("Response "+response['telephone_number']);
            $("#telephone_number").val("");
            $('#add_phone_modal').modal('hide')
            $("#contact_list").append( '<li id="tel_'+response['id']+'">'+response['telephone_number']+'<a href="#" data-id="'+response['id']+'">&nbsp; &nbsp;<i class="icon-remove text-error"></i></a></li>' );
          }
        },
        error: function(error){
        }
      });
   });

   $("#btn_edit_place").on("click", function(event){
      event.preventDefault();
      place_name = $('span#place_name').html();
      $('#messge_error_place_update').html('');
      $('#update_place_modal_label').html(place_name);
      $('#place_name_update').val(place_name)
      $('#update_place_modal').modal('show');

      tmp_parent_id = $('#'+current_place).parents('li').attr('id');
      $.ajax({
          type: 'GET',
          url:'/places/get_parent_level/',
          data: {current_place_id: current_place},
          success: function(response) {               
            set_opt_select_edit_place(response, tmp_parent_id, 'place_parent_id');
          },
      });         
    
   });

   $("#btn_delete_place").on("click", function(event){
      $("#confirm_modal").modal('show');
      $('#confirm_modal').on('shown', function () {
        $("#btn_confirm_yes").on("click", function(){
          $.ajax({
            type: "DELETE",
            url:'/places/delete_place', 
            data: {id: current_place},
            success: function(response){   
              console.log(response); 
              $("#confirm_modal").modal('hide');
              $("span#place_name").html("");
              $("#btn_edit_place").hide();
              $("#btn_delete_place").hide();
              $("#btn_add_phone").hide();
              $("#contact_list").empty();

              $('li#'+current_place).remove();
            },
            error: function(response){
              console.log("error"+response);
            }        
          });
        });

      });

   });


  $("#btn_submit_place").on("click",function(event){
    event.preventDefault();
    place_name = $('#place_name').val();
    place_type = $('#txt_type').val();
    place_parent_id = $('#parent_id').val();
    
    $.ajax({
          type: 'POST',
          url:'places/create_place/',
          data: {name:place_name, type:place_type, parent_id:place_parent_id},
          success: function(response) { 
            console.log(response);
            if(jQuery.isArray(response)){
             // console.log(response);
              tmp = '';
              $.each( response, function( key, data ) {
                tmp = tmp + data;
              });
             $('#messge_error_place').html(tmp);
            }else{
              $("#place_tree").html(response);
               $('#add_place_modal').modal('hide')  ; 
            }
             
          },
          error: function(error){
            console.log(error);
          }
    });
  });



  $("#btn_update_place").on("click",function(event){
    event.preventDefault();
    place_name_update = $('#place_name_update').val();
    place_parent_id = $('#place_parent_id').val();
    if(place_parent_id != null){
      $.ajax({
            type: 'GET',
            url:'/places/update_place/',
            data: {id: current_place, name:place_name_update, parent_id: place_parent_id},
            success: function(response) { 
              handle_update_place_response(response);
            },
            error: function(error){
              console.log(error);
            }
      });
    }else{
      $.ajax({
            type: 'GET',
            url:'/places/update_place/',
            data: {id: current_place, name:place_name_update},
            success: function(response) { 
              handle_update_place_response(response);
            },
            error: function(error){
              console.log(error);
            }
      });
    }
  });

  $('#btn_add_phone').on("click", function(event){
    clear_add_telephone_modal();
  });

  $(document.body).on("click","#contact_list>li>a", function(event){
    event.preventDefault();
    id = $(this).attr('data-id');
    delete_contact(id);
  });
});
  function clear_add_telephone_modal(){
    $('#messge_error_telephone_number ul').empty();
    $('#telephone_number').val('');
  }
  function handle_update_place_response(response){
    if(jQuery.isArray(response)){
      tmp = '';
      $.each( response, function( key, data ) {
        tmp = tmp + data;
      });
     $('#messge_error_place_update').html(tmp);
    }else{
      $("#place_tree").html(response);
      $('#update_place_modal').modal('hide')  ; 
      $('span#place_name').html(place_name_update);
      $('#messge_error_place_update').html('');
    }    
  }

  function set_opt_select_edit_place(response, current_parent_id, selector){
    $('#'+selector).empty();
    if(response == null){
      $('#sel_parent_id').hide();
    }else{
      $('#sel_parent_id').show();
      $.each( response, function( key, data ) {
        if(data['id'] == current_parent_id)
          $('#'+selector).append('<option value="'+data['id']+'" selected>'+data['name']+'</option>');
        else
          $('#'+selector).append('<option value="'+data['id']+'">'+data['name']+'</option>');
      }); 
    }
  }

  function set_state(current_place_type, current_place_parent_id, current_place_label){
    $("#txt_type").val(current_place_type);
    $('#messge_error_place').html('');
    $('#place_name').val('');
    $('#add_place_modal_label').html(current_place_label);    

    if(current_place_type == 'Phd'){
      $('#div_parent_place').hide();
    }else{
      $('#div_parent_place').show();
      $.ajax({
        type: 'GET',
        url:'/places/get_sibling_level/',
        data: {current_place_id: current_place_parent_id},
        success: function(response) {               
          // console.log(response);
          set_opt_select_edit_place(response, current_place_parent_id, 'parent_id');
        },
      }); 
      
    }



  }

  function show_btn_od(){
    if(window.current_user_place_type == "Phd" || window.current_user_place_type == '')
      $("#btn_add_od").show();
    $("#btn_add_hc").hide();
  }

  function show_btn_hc(){
    // console.log(window.current_user_place_type);
    if(window.current_user_place_type == "Phd" || window.current_user_place_type == ''){      
      $("#btn_add_od").show();
      $("#btn_add_hc").show();
    }
    if(window.current_user_place_type == "Od")
      $("#btn_add_hc").show();
  }

  function enable_place_action(current_place){
    if($('#'+window.current_user_place_id).has($('#'+current_place)).length > 0 || window.current_user_place_id == ''){
      $("#btn_edit_place").show();
      $("#btn_delete_place").show();    
    }else if(window.current_user_place_id == current_place){
      $("#btn_edit_place").show();
      $("#btn_delete_place").hide();
    }else{
      $("#btn_edit_place").hide();
      $("#btn_delete_place").hide();      
    }
  }

	function set_place_phd(val1,val2){
		phd_id = val1;
   
    current_place = phd_id;
    $("#contact_list").empty();
    $(".contact_label").empty();
    $("#btn_add_phone").hide();
    $("#place_header").show();
    $("span#place_name").html(val2);
    enable_place_action(current_place);
	}
	function set_place_od(val1,val2){
		od_id = val1;
    current_place = od_id;
    $("#contact_list").empty();
    $(".contact_label").empty();
    $("#btn_add_phone").hide();
    $("#place_header").show();  
    $("span#place_name").html(val2);
    enable_place_action(current_place);

	}
  function show_contact(val1,val2){
    current_place = val1;
    b = true;
    $("span#place_name").html(val2);
    $("#place_header").show();
    $(".contact").show();
    $('#place_name').val('');
    // alert(current_place);
    if($('#'+window.current_user_place_id).has($('#'+current_place)).length > 0 || window.current_user_place_id == ''){
      $("#btn_edit_place").show();
      $("#btn_delete_place").show();
      $("#btn_add_phone").show();
      $(".btn_delete_contact").show();
    }else{
      b = false;
      $("#btn_edit_place").hide();
      $("#btn_delete_place").hide();
      $("#btn_add_phone").hide();    
      
    }
    $.ajax('/places/'+val1+'/contacts', {
          dataType: "json",
          type: 'GET',
          data: {},
          success: function(response) { 
            // console.log(response);
            $("#contact_list").empty();     
            $('.contact_label').empty();       
            if(response.length > 0){
              $("#contact_list").before("<h4 class='contact_label'>Contact</h4>");
            }
                       

            $.each( response, function( key, data ) { 
              $("#contact_list").append( "<li id='tel_"+data['id']+"'>"+data['telephone_number']+
                "&nbsp;&nbsp;&nbsp;<a href='#' class='btn_delete_contact' data-id='"+data['id']+"'><i class='icon-remove text-error'></i></a></li>" );
            });

            if(b == false)
              $(".btn_delete_contact").hide(); 
          }
    });
  }

  function delete_contact(val){
    $("#confirm_modal").modal('show');
    $('#confirm_modal').on('shown', function () {
        $("#btn_confirm_yes").on("click", function(){
          $.ajax({
            type: "DELETE",
            url:'/contacts/delete_contact', 
            data: {id: val},
            success: function(response){ 
              console.log(response);   
              $("#confirm_modal").modal('hide');
              $('li#tel_'+val).remove();
            },
            error: function(error){
            }        
          });
        });
    });
  }
