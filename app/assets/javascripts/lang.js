window.lang = {};
window.lang["en"] = {
  "required_field": "This field is required.",
  "incorrect_date_range": "Incorrect date range, please check it again !",
};

window.lang["kh"] = {
  "required_field": "មិនអាចទទេរទេ",
  "incorrect_date_range": "មានទំរង់មិនត្រឹមត្រូវទេ",		
}