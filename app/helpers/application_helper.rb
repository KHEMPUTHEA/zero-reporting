module ApplicationHelper
	def javascript(*files)
  		content_for(:head) { javascript_include_tag(*files) }
	end

	def select_place user, name, id
	  options = options_for_place current_user
	  options_str = options.map do |option|
	  	if !user.nil? && option[1] == user.place_id 
	  		content_tag :option, option[0].html_safe, selected: '', value: option[1]
	  	else
	  		content_tag :option, option[0].html_safe, value: option[1]
	  	end
	  end.join("").html_safe

	  content_tag :select , options_str, name: name, id: id

	end

end
