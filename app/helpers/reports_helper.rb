module ReportsHelper
  def link_calendar_to date, report_by, step = 1
    if report_by === "Weekly"
      from_date = date + (step * 7).days
      to_date = from_date + 6.days
    elsif report_by === "Monthly"
      from_date = Date.new(date.year, date.mon, 1) + step.month
      to_date = from_date + 1.month - 1.days
    else
      from_date = Date.new(date.year + step, 1, 1)
      to_date = from_date + 1.year - 1.days
    end

    link_to step == 1? ">" : "<", reports_path(:from => from_date, :to => to_date, :report_by => report_by, :previous_view => @previous_view), :title => from_date
  end

  def crosstab rows, columns, values
    crosstab = Crosstab.generate rows, columns, values

    html_string = "<table class='table table-hover'>"

    # generate table headers
    html_string += "<thead>"
    crosstab[:headers].each do |column|
      html_string += "<th title=#{column}>" + column +"</th>"
    end
    html_string += "</thead>"

    # generate table body
    html_string += "<tbody>"
    td_style = ""
    tr_style = ""
    crosstab[:bodies].each do |values|
      values.compact.length > 1 ? tr_style = "": tr_style = "missedReport"
      html_string += "<tr class=#{tr_style}>"
      values.each do |value|
        if value.kind_of? Integer
          value.eql?(0) ? td_style = "zeroValue": td_style = "nonZeroValue"
        else
          td_style = "disease"
        end
        html_string += "<td title=#{value} class='#{td_style}'>#{value}</td>"
      end
      html_string += "</tr>"
    end

    # generate total reporting cases
    html_string += "<tr class='footer'>"
    crosstab[:footers].each do |value|
      if value.kind_of? Integer
          value.eql?(0) ? td_style = "zeroValue": td_style = "nonZeroValue"
        else
          td_style = "total"
      end
      html_string += "<td class='#{td_style}'>#{value}</td>"
    end
    html_string += "</tr>"

    html_string += "</tbody>"
    html_string += "</table>"
    html_string.html_safe
  end
end