require "csv"
require 'date'
class ReportsController < ApplicationController

  skip_before_filter :check_authentication, :only => [:create] 

  def index

    date = (params[:from].present?) ? Date.strptime(params[:from], "%Y-%m-%d") : Time.new.to_date    
    @previous_view = params[:report_by]
    @report_by = params[:report_by].present? ? params[:report_by] : "Weekly"

    date_range = date.to_range(@report_by)
    @week_number = date_range.number if date_range.kind_of? Week
    @from_date = date_range.from_date  
    @to_date = date_range.to_date

    if(current_user.level == 1)
      sub_places = Phd.all
      @place_type = nil
    else
      sub_places = current_user.place.children.all
      @place_type = current_user.place.type
    end

    if params[:places].present?
      @places = params[:places]
    else
      @places = []
      sub_places.each do |p|
        @places << p.id
      end
    end

    @time_range = @from_date.to_s+" - "+@to_date.to_s
    @diseases = Disease.by_name.sort
    @diseases.delete('No')

    @reports = Report.total_cases_by_disease_place_date current_user, @from_date, @to_date
    @rows  = create_table @reports, @diseases, sub_places, @places

    @places.delete(0) 
    @places.delete("0") 
    

    results = Report.search_by_place_date @places, @from_date, @to_date, @place_type    
    @places_name = Place.get_places_name @places
   
    if !results.nil? 
      @data_source = Report.create_data_source results 
    end
    
    if request.xhr?
       render 'index_ajax', layout:false
    end

  end



  def create
    p request.raw_post
    report_cases = RWCaseParser.parse request.raw_post
    p report_cases

    user = User.find_by_health_center_contact params[:from] if params[:from]
    # user = User.find_by_phone_number_of_reporting_wheel params[:from] if params[:from]

    if !user.nil? and user.hc?
      Report.handle_update_and_create user.place_id, report_cases, params
    else
      raise Exception.new("User does not exist!")
    end
    render layout:false
  end


  def export_csv 
    from_date = Date.strptime(params[:from], "%Y-%m-%d") if params[:from].present?
    to_date = Date.strptime(params[:to], "%Y-%m-%d") if params[:to].present? 
    
    if current_user.level == 1
      places = Phd.all
    else
      places = current_user.place.children.all
    end
    diseases = Disease.by_name.sort
    diseases.delete('No')

    results = Report.total_cases_by_disease_place_date current_user, from_date, to_date
    rows  = create_csv_data_row results, diseases, places

    from_date = from_date.strftime("%d-%m-%Y")
    to_date = to_date.strftime("%d-%m-%Y")

    csv_string = CSV.generate do |csv|
      rows.each do |r|
        csv << r
      end
      csv
    end

    send_data csv_string,
      :type => 'text/csv; charset=iso-8859-1; header=present',
      :disposition => "attachment; filename = #{from_date}_to_#{to_date}_zero_report.csv"
  end

  def create_table_head diseases
    row = []
    if(current_user.level == 1)
      name = t('place.provincial_health_department')
    else
      name =  current_user.place.child_class == Od ? t("place.operational_district") : t("place.health_center")
    end
    row << name
    row + diseases
  end

  def create_csv_data_row reports, diseases, places
    header = create_table_head(diseases)
    
    rows = [header]
    footer_row = []
    header.each do |h|
      footer_row << 0
    end
   
    places.each do |place|      
      row = [place.name]
      diseases.each_with_index do |disease, i|         
        value = Report.search_disease_in_report(reports, disease, place.id) 
        row << value        
        footer_row[i+1] = footer_row[i+1] + value
      end
      rows << row
    end
    
    footer_row[0] = t('report.total')   
    rows << footer_row
  end

  def create_table reports, diseases, places, checked_places
    header = create_table_head(diseases)
    
    rows = [header]
    footer_row = ["",""]
    header.each do |h|
      footer_row << 0
    end
   
    places.each do |place|      
      row = [place.id]
      row << place.name
      diseases.each_with_index do |disease, i|         
        value = Report.search_disease_in_report(reports, disease, place.id) 
        row << value
        if checked_places.include?(place.id) ||  checked_places.include?(place.id.to_s)
          footer_row[i+2] = footer_row[i+2] + value
        end
      end
      rows << row
    end
    
    footer_row[0] = ""
    footer_row[1] = t('report.total') 
    footer_row.delete_at(footer_row.length-1)

    rows << footer_row
  end


  def hc_chart  
    @hcs = Hc.all
    @place_id = params[:place_id]
    @start_date = params[:start_date]
    @end_date = params[:end_date]
  end  

  def create_hc_chart
    @hcs = Hc.all

    place_id = params[:place_id]
    start_date = params[:start_date].to_date
    end_date = params[:end_date].to_date

    if Report.validate_date_range(start_date,end_date)
      results = Report.chart_by_place_date place_id, start_date, end_date, current_user.place.type
    else
      results = nil
    end

    respond_to do |format|
      format.json  { render :json => results }      
      format.html
    end    
  end

  def disease_chart
    @diseases = Disease.by_name.sort
    @diseases.delete('No')
    @places = current_user.place.children.all
  end

  def create_disease_chart
    places = params[:places]
    start_date = params[:start_date].to_date
    end_date = params[:end_date].to_date
    diseases = params[:diseases]

    if Report.validate_date_range(start_date,end_date)
      results = Report.chart_by_place_date_disease places, start_date, end_date, diseases
    else
      results = nil
    end

    respond_to do |format|
      format.json  { render :json => results }      
      format.html
    end   
  end


end
