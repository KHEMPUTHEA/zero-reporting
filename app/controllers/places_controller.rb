class PlacesController < ApplicationController

  def search_place_name
    @results = Place.where("name LIKE ?", params[:name])
        respond_to do |format|
          format.json  { render :json => @results }      
    end
  end

  def get_children
    @parent_id = params[:id]
    @places = Place.where(:parent_id => @parent_id)
    respond_to do |format|
          format.json  { render :json => @places }      
    end
  end

  def dynamic_ods
    @ods = Od.find(:all)
  end

  def for_sectionid
    @subsections = SubSection.find( :all, :conditions => [" section_id = ?", params[:id]]  ).sort_by{ |k| k['name'] }    
    respond_to do |format|
      format.json  { render :json => @subsections }      
    end
  end
  
  def index
    @places = Phd.order(:name)
  end
  



  def show
    place = Place.find(params[:id])
    @place_user = place.users
    respond_to do |format|
      format.json { render json: @place_user }
    end
  end

  def destroy
    @place = Place.find(params[:id])
    @place.destroy
  end

  def create_place
    place = Place.new name: params[:name], type: params[:type], parent_id: params[:parent_id]

    if current_user.level == 2
      if current_user.place.type == 'Phd'
        if params[:type] == 'Phd'
          place.errors.add(:type, "is invalid")
        end
      elsif current_user.place.type == 'Od'
        if params[:type] == 'Phd' || params[:type] == 'Od'
          place.errors.add(:type, "is invalid")
        end
      end
    end
    
    # place.save
    @places = Phd.order(:name)
    @current_place_id = Place.last.id
    # tmp = current_user.name
    
    if place.errors.size == 0 && place.save      
      if request.xhr?
        render '_list', layout:false
      end
    else
      respond_to do |format|
        format.json { render json: place.errors.full_messages }
      end  
    end
  end

  def delete_place
    place = Place.find(params[:id])
    access_key = true
    if current_user.level == 2
      if place.parent_id == current_user.place_id || current_user.place.children.find(place.parent_id)
        access_key = true 
      else
        access_key = false
      end
    end 

    if access_key 
      response = {success: place.destroy}
      # response = {error: place.errors.full_messages}
    else
      place.errors.add('Unknow', 'Error')
      response = place.errors.full_messages
    end
    render json: response
  end




  def update_place
    @place = Place.find(params[:id])
    # if @place.id != current_user.place_id

    access_key = enable_access_level(@place)
    if access_key
      @places = Phd.order(:name)
      if !params[:parent_id].present?
        parent_id = nil
      else
        parent_id = params[:parent_id]
      end
      @current_place_id = params[:id]
    else
      @place.errors.add("Unknow", "Error");
    end


    if access_key == true && @place.update_attributes(:name => params[:name], :parent_id => parent_id)
      if request.xhr?
        render '_list', layout:false
      end
    else
      respond_to do |format|
        format.json { render json: @place.errors.full_messages }
      end 
    end
  end


  def get_parent_level
    place_id = params[:current_place_id]
    place = Place.find(place_id)
    if place.type == 'Phd'
      result = nil
    else
      od = Place.find(place.parent_id)
      result = Place.where(:parent_id => od.parent_id)
    end

    respond_to do |format|
      format.json { render json:  result}
    end    
  end

  def get_sibling_level
    place_id = params[:current_place_id]
    place = Place.find(place_id)
    result = Place.where(:parent_id => place.parent_id)

    respond_to do |format|
      format.json { render json:  result}
    end     
  end


end
