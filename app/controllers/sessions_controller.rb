class SessionsController < ApplicationController
  skip_before_filter :check_authentication
  
  def new
    if current_user
      redirect_to after_login_path
    end
  end

  def create
    # if !params[:name].present?
    #   redirect_to root_url, :flash => { :error => "Enter your username." }
    #   return
    # end

    # if !params[:password].present?
    #   redirect_to root_url, :flash => {:error => "Enter your password.", :name => params[:name] }
    #   return
    # end

    
    if user = User.authenticate(params[:name], params[:password])
      session[:user_id] = user.id
      redirect_to  after_login_path
    else
      flash.now["error"] = "Username or password you entered is incorrect."
      render :new 
    end

  end
  
  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end
end
