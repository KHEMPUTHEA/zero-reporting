class ContactsController < ApplicationController
  def index
    @place = Place.find(params[:place_id])
    @contacts = @place.contacts

    respond_to do |format|
      format.json  { render :json => @contacts }      
    end
  end

  def new
    @place = Place.find(params[:place_id])
  	@contact = @place.contacts.build
  end

  def create_contact
    contact = Contact.new telephone_number: params[:telephone_number].strip, place_id: params[:place_id]
    # response = {success: contact.save}
    if contact.save == true
     render json: contact
    else
      p contact.errors.full_messages
      render json: contact.errors.full_messages
    end
     
  end

  def create    
    
    @place = Place.find(params[:place_id])  
    @contact = @place.contacts.build(:telephone_number => params[:telephone_number].strip)
    result = @contact.save
   
    if result
      flash['success'] = "successfully"
      redirect_to place_contacts_path(@place)
    else
      flash.now[:error] = "Error"
      render action: "new"
    end
 

    
  end

  def destroy
    @place = Place.find(params[:place_id])
    # @contact = Contact.where(:telephone_number => params[:telephone_number], :place_id => params[:place_id])
    @contact = Contact.find(params[:id])
    @contact.destroy

    redirect_to place_contacts_url(@place.id), notice: 'Contact was successfully deleted.'    
  end

  def delete_contact
    contact = Contact.find(params[:id])
    place = Place.find(contact.place_id)
    access_key = enable_access_level(place)
    if access_key
      if !params[:parent_id].present?
        parent_id = nil
      else
        parent_id = params[:parent_id]
      end
      @current_place_id = params[:id]
      response = contact.destroy
    else
      response = place.errors.add("Unknow", "Error");
    end
    
    render json: response    
  end
end
