class PlaceTypesController < ApplicationController
  def index
  	@place_types = PlaceType.includes('places').all
  end
end
