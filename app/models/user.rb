class User < ActiveRecord::Base
  
  ADMIN_USER =  1
  DEFAULT_USER = 2
  belongs_to :place
  # has_many :reports, dependent: :destroy
  
  # attr_accessor :old_password, :password_confirmation
  has_secure_password
  attr_accessible :name, :password, :place_id, :can_login, :phone_number, :old_password, :password_confirmation, :level,
                  :created_at, :updated_at

  validates :name, :presence => true
  validates :name, :uniqueness => true   
  validates :place_id, :presence => true, :if => lambda { |user| user.level != ADMIN_USER}
  # validates :phone_number, :presence => true, :if => lambda { |user| user.level != ADMIN_USER}
  validates :password_confirmation, :presence =>true ,:on => :change_password
  # validates :phone_number, format: { with: /^(\+\d{1,3}\s)?\(?\d{2,3}\)?([\s.-]?\d+)*$/, message: :bad_format }, :if => lambda { |user| user.level != ADMIN_USER}

  
  def self.authenticate(name, password)
    user = User.find_by_name(name)

    if !user.nil? && user.can_login
      if user && user.authenticate(password)
        user
      else
        false
      end 
    # else
    #    self.errors.add(:can_login, "No authenticate" )
    #    return false
    end   
  end

  # def validate!
  #   errors.add(:can_login, "cannot be nil") if can_login == false
  # end

  def self.can_login?
    where(:can_login => true)
  end

  def is_member user
    if self.level == 1
      self.members.exists?(user.id)
    else
      self.members.include?(user)
    end
  end

  def members
    if self.level == 1
      @users = User.where('level not in (?)', [1]).order(:name)     
    else
      @users = User.where(:place_id => self.place.children.select(:id))
      if self.place.type == 'Phd'        
        @users.each do |u|
           @users = @users + User.where(:place_id => u.place.children.select(:id))
        end 
      end
      @users.sort_by { |user| user[:name] }
    end
  end

  # def member
  #   User.where(:place_id => self.place.children.select(:id)).order(:name)
  # end

  def provincial?
    self.place && self.place.type == "Phd"
  end

  def od?
    self.place && self.place.type == "Od"
  end

  def hc?
    self.place && self.place.type == "Hc"
  end

  def change_password(params)
    if(self.authenticate(params[:old_password]))
      if params[:password] != ""
        self.password = params[:password]
        self.password_confirmation = params[:password_confirmation]
        return self.save
      else
        self.errors.add(:password, I18n.t('activerecord.errors.models.user.attributes.password.blank'))
        return false
      end
    else
      self.errors.add(:old_password, I18n.t('activerecord.errors.models.user.attributes.old_password.not_match'))
      return false
    end    
  end

  def self.find_by_phone_number_of_reporting_wheel phone_number_rw
    sender_phone_number = phone_number_rw.gsub(/(sms:\/\/)/, "") if phone_number_rw
    User.find_by_phone_number sender_phone_number
  end

  def self.find_by_health_center_contact phone_number_rw
    sender_phone_number = phone_number_rw.gsub(/(sms:\/\/)/, "") if phone_number_rw
    sender_place_id = Contact.find_by_telephone_number(sender_phone_number).place_id
    sender = User.find_by_place_id(sender_place_id)
  end

  def self.find_by_name_ignore_case name
    User.find(:first, :conditions => ["lower(name) = ?", name.downcase])
  end


  def self.construct_place places, result_opts, space
    
    result_opts = result_opts || []
    if places.class != Array
      places = [places]
    end
    count_space = space * 5 
    space = space + 1
    places.each do |place|
      n_space = ""
      nbsp = "&nbsp;"
      n_space = nbsp* count_space      
      result_opts << [n_space + place.name,place.id]      
      construct_place(place.children.to_a, result_opts, space)
    end    
  end

end
