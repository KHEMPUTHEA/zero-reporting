module Old
	class Report < Keansvay
	  attr_accessible :created_at, :disease, :from, :lat, :lon, :quantity, :sender, :to, :user_id, :place_id
	  belongs_to :user
	  belongs_to :place
	end
end