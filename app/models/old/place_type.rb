module Old
	class PlaceType < Keansvay
	  has_many :places, :dependent => :destroy
	  attr_accessible :code, :description
	end

end