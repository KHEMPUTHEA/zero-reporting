module Old
	class User < Keansvay
		belongs_to :place
		has_many :reports

		attr_accessible :name, :password, :place_id, :can_login, :phone_number

		validates :place_id, :presence => true
		validates :name, :presence => true
		validates :name, :uniqueness => true


		def provincial?
			self.place && self.place.place_type.code == "PHD"
		end

		def od?
			self.place && self.place.place_type.code == "OD"
		end

		def hc?
			self.place && self.place.place_type.code == "HC"
		end
	
	end

end