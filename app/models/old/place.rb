module Old
	class Place < Keansvay
	  belongs_to :place_type
	  belongs_to :parent, :class_name => "Place"

	  has_many :users

	  attr_accessible :name, :parent_id, :place_type_id

	  def children
	    Place.where(:parent_id => self.id)
	  end
	end

end