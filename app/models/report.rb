class Report < ActiveRecord::Base
  attr_accessible :created_at, :disease, :from, :lat, :lon, :quantity, :sender, :to, :user_id, :place_id
  belongs_to :user
  belongs_to :place

  REPORT_TYPE = {:weekly => "Weekly", :monthly => "Monthly", :yearly => "Yearly"}
  
  def self.between from_date, to_date
    where(:created_at => from_date..to_date+1)
  end

  public 
  def self.child_place_of place
    joins(:place).where("places.parent_id" => place.id)
  end

  def self.from_disease disease
    where(:disease => disease)
  end

  def self.find_by_place place_id
    where(:place_id => place_id)
  end

  def self.find_by_place_disease_and_date(place_id, disease, from_date, to_date)
    # records = Report.from_disease(disease).find_by_place(user)
    records = Report.from_disease(disease).between(from_date,to_date).where(:place_id => place_id)
    if records.count > 0
      records[0]
    else
      nil
    end
  end

  def self.find_no_disease report_cases
    report_cases.select{|disease, quantity| disease == "No_Disease"}.count
  end

  def self.handle_update_and_create report_place_id, report_cases, options
    date = Time.now.to_date
    date_range = date.to_range("Weekly")
    from_date = date_range.from_date
    to_date = date_range.to_date
    no_disease = Report.find_no_disease report_cases

    # user = User.find_by_name_ignore_case options[:sender]

    if no_disease > 0
      report = Report.update_all("quantity = 0", :created_at => from_date...to_date, :place_id => report_place_id)
    end

    report_cases.each do |disease, quantity|
      record = Report.find_by_place_disease_and_date(report_place_id, disease, from_date, to_date)
     
      if !record.nil?
        record.quantity = (no_disease > 0) ? 0 : quantity
        record.save        
      else
        Report.create(:sender => options[:sender],
          :disease => disease, :quantity => quantity, :from => options[:from],
          :lat => options[:lat], :lon => options[:lon], :to => options[:to], :place_id => report_place_id)
      end
    end   
  end
  
  def self.total_cases_by_disease_place_date user, from_date, to_date
    if user.level == 2
      place_id = user.place_id
      p_type = Place.find(place_id).type
    end

    if p_type == "Od"
      results = joins(:place).where("places.parent_id" => place_id).between(from_date, to_date+1.days).group("places.id", "reports.disease").sum(:quantity)
    elsif p_type == "Phd"
      p_ods = Place.where("parent_id" => place_id)
      results = Hash.new
      p_ods.each do |p_od|
        tmp = joins(:place).where("places.parent_id" => p_od.id).between(from_date, to_date+1.days).group( "places.parent_id","reports.disease").sum(:quantity)
        tmp.each do |key,value|
          results[key] = value
        end
      end
    else
      p_phds = Phd.all
      results = Hash.new
      p_phds.each do |p_phd|
        p_ods = p_phd.children
        arr = []
        p_ods.each do |p_od|
          arr << p_od.id
        end
        tmp = joins(:place).where("places.parent_id" => arr).between(from_date, to_date+1.days).group("reports.disease").sum(:quantity)
        tmp.each do |key,value|
          arr_key = [p_phd.id.to_s]
          arr_key << key
          results[arr_key] = value
        end     
      end
    end
    results
  end


  def self.total_cases_by_disease_place place_id
    p_type = Place.find(place_id).type
    if p_type == "Od"
      results = joins(:place).where("places.parent_id" => place_id).group("places.id", "reports.disease").sum(:quantity)
    else
      p_ods = Place.where("parent_id" => place_id)
      results = Hash.new
      p_ods.each do |p_od|    
        tmp = joins(:place).where("places.parent_id" => p_od.id).group( "places.parent_id","reports.disease").sum(:quantity)
        tmp.each do |key,value|
          results[key] = value
        end
      end
    end
    results
  end
  




  def self.search_disease_in_report reports, disease, place_id
    reports.each do |key,value|
        if key[0].to_s == place_id.to_s && key[1] == disease
          return value
        end
    end    
    return 0
  end


  def self.search_by_place_date places_id, from_date, to_date, current_place_type
    if current_place_type.nil?
      results = Hash.new
      places = places_id
      places.each do |place|
        ods = Place.find(place).children
        arr_place = []
        ods.each do |od|
          arr_place << od.id
        end
        tmp = joins(:place).where("places.parent_id" => arr_place).between(from_date, to_date+1.days).select("reports.created_at").group('reports.disease').sum(:quantity)
        
        tmp.each do |key,value|
          if results.has_key? key
            results[key] = results[key] + value
          else
            results[key] = value
             
          end
        end
      end
    elsif current_place_type  == 'Od'
      results = joins(:place).where("places.id" => places_id).between(from_date, to_date+1.days).select("reports.created_at").group('reports.disease').sum(:quantity)
    else
      results = joins(:place).where("places.parent_id" => places_id).between(from_date, to_date+1.days).select("reports.created_at").group('reports.disease').sum(:quantity)
    end    
    results
  end

  def self.chart_by_place_date places_id, from_date, to_date, current_place_type
    if current_place_type  == 'Od'
      results = joins(:place).where("places.id" => places_id).between(from_date, to_date+1.days).select("reports.created_at, reports.disease, sum(quantity) quantity").group("reports.created_at","reports.disease").order('created_at')
    else
      results = joins(:place).where("places.parent_id" => places_id).between(from_date, to_date).select("reports.created_at, reports.disease, sum(quantity) quantity").group("reports.created_at","reports.disease").order('created_at')
    end    
    results
  end

  def self.chart_by_place_date_disease  places_id, from_date, to_date, diseases
    results = joins(:place).where("places.id" => places_id, "reports.disease" => diseases).between(from_date, to_date+1.days).select("reports.created_at, places.name, sum(quantity) quantity").group("reports.created_at","places.name").order('created_at')
  end

  def self.create_data_source data
    arr = []
    data.each do |key,value|
      tmp = [key]
      tmp << value
      arr << tmp
      tmp = []
    end
    arr
  end

  def self.validate_date_range from_date, to_date
    if from_date < to_date
      return true
    end
    return false
  end

end

