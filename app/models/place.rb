class Place < ActiveRecord::Base
  belongs_to :parent, :class_name => "Place"

  has_many :users, dependent: :destroy
  has_many :contacts, dependent: :destroy
  has_many :reports, dependent: :destroy
  attr_accessor :phd_id
  attr_accessible :name, :parent_id , :type, :created_at, :updated_at
	validates :name, :presence => true
  validates :name, :uniqueness => true

# def self.search(search)
#   if search
#     where('name LIKE ?', "%#{search}%")
#   else
#     scoped
#   end
# end

  # def get_parent_name parent_id
  #   Place.find(parent_id).name
  # end
   
  def self.import_place place
    default_password = "123456"
    place_class = place.place_type.code.capitalize.constantize
      place_new = place_class.new(:name => place.name, :parent_id => place.parent_id, :created_at => place.created_at, :updated_at => place.updated_at)

      if place_new.save
           
      else
        place_new  =  Place.find_by_name(place_new.name)  
      end
      place.users.each do |user|
        password = user.password.empty? ? default_password : user.password
        
        user_new = place_new.users.build(:name => user.name, :password => password,
         :password_confirmation => password, :can_login => user.can_login, :phone_number => user.phone_number,
         :created_at => user.created_at, :updated_at => user.updated_at)
        
        if user_new.save
          contact_new = place_new.contacts.build(:telephone_number => user.phone_number, :place_id => user_new.place_id, :created_at => user.created_at, :updated_at => user.updated_at)
          if contact_new.save
          
          else
            p contact_new.errors.full_messages
          end
          user.reports.each do |report|
            report_new = Report.new(:sender => report.sender, :disease => report.disease, :quantity => report.quantity,
              :from => report.from, :lat => report.lat, :lon => report.lon, :to => report.to, :place_id => place_new.id, :created_at => report.created_at)
            if report_new.save
              
            else
              p report_new.errors.full_messages
            end
          end
        else
          p user_new.errors.full_messages
        end        
      end 

      # p User.all
  end


  def children
    Place.where(:parent_id => self.id).order(:name)
  end

  def child_class
    raise "You must implement this method : child_class"
  end
  
  def self.inherited(child)
    child.instance_eval do
      def model_name
        Place.model_name
      end
    end
    super
  end

  def self.get_places_name places
    places_name = []
    places.each do |p_id|      
      places_name << Place.find(p_id).name      
    end    
    places_name
  end



end
