class Disease
  attr_accessor :name

  DISEASES = [{id: 1, name: "AWD"}, {id: 2, name: "F-R"}, {id: 3, name: "AFP"}, {id: 4, name: "ARI"}, {id: 5, name: "DHF"}, {id: 6, name: "ME"}, {id: 7, name: "AJ"}, {id: 8, name: "DPT"}, {id: 9, name: "Rab"}, {id: 10, name: "NT"}, {id: 11, name: "MD"}, {id: 12, name: "Other"}, {id: 13, name: "No"}]
  def self.all
    DISEASES
  end

  def self.by_name
  	row = []
  	DISEASES.each do |d|
       row << d[:name]
    end
    return row
  end


end