class Week
  START_DATE_OF_WEEK = Date.new(2011, 12, 28)
  def initialize date
    @date = date
  end

  # from_date = date - number_day_over_of_week
  def from_date
    @date - number_days_over_of_week.days
  end

  def to_date
    self.from_date + 6.days
  end

  # number = (date - start_date_of_week) / 7days modulo 52
  def number
    ((((@date + 1.days) - START_DATE_OF_WEEK) / 7) % 52).ceil
  end
  # number_day_over_of_week = (date - start_date_of_week) mod 7days
  def number_days_over_of_week
    ((@date - START_DATE_OF_WEEK) % 7)
  end

end