class Contact < ActiveRecord::Base
	belongs_to :place

  	attr_accessible :place_id, :telephone_number, :created_at, :updated_at
  	
  	validates :telephone_number, :presence => true
  	validates :telephone_number, :uniqueness => true
	validates :telephone_number, format: { with: /^(\+\d{1,3}\s)?\(?\d{2,3}\)?([\s.-]?\d+)*$/, message: :bad_format }
end
