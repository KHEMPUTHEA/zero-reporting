class Year
  def initialize date
    @date = date
  end

  def from_date
    Date.new(@date.year, 1, 1)
  end

  def to_date
    Date.new(@date.year + 1, 1, 1) - 1.days
  end
end