require 'spec_helper'

describe Place do
  pending "add some examples to (or delete) #{__FILE__}"

  describe "children" do
	  before(:each)do
	    @phd_1 = Phd.create(:name => "PHD 1")
	    @phd_2 = Phd.create(:name => "PHD 2")

	    @od_1 = Od.create(:name => "OD 1", :parent_id => @phd_1.id)
	    @od_2 = Od.create(:name => "OD 2", :parent_id => @phd_1.id)

	    @hc_1 = Hc.create(:name => "HC 1", :parent_id => @od_1.id)
	    @hc_2 = Hc.create(:name => "HC 2", :parent_id => @od_1.id)

	    @hc_3 = Hc.create(:name => "HC 3", :parent_id => @od_2.id)


	    @od_3 = Od.create(:name => "OD 3", :parent_id => @phd_2.id)
	    @hc_3 = Hc.create(:name => "HC 3", :parent_id => @od_3.id)
	  end

	  it "return children by parent place" do
	  	phd_child = @phd_1.children
	  	phd_child.count.should eq  2
	  	phd_child[0].name.should eq "OD 1"
	  	phd_child[1].name.should eq "OD 2"


	  	od1_child = @od_1.children
	  	od1_child.count.should eq 2
	  	od1_child[0].name.should eq "HC 1"
	  	od1_child[1].name.should eq "HC 2"

	  	od2_child = @od_2.children
	  	od2_child.count.should eq 1
	  	od2_child[0].name.should eq "HC 3"
	  end
  end

  describe ".import_place" do
  	before(:each) do
		phd_1 = Phd.create(:name => "PHD 1")
		od_1 = Od.create(:name => "OD 1", :parent_id => phd_1.id)
		
		
		hc_1 = Hc.create(:name => "HC 1", :parent_id => od_1.id)
		user_1 = User.create(:name => 'User1 HC 1', :password => '123', :password_confirmation => '123', :place_id => hc_1.id)
		user_2 = User.create(:name => 'User2 HC 1', :password => '123', :password_confirmation => '123', :place_id => hc_1.id)
		
		hc_2 = Hc.create(:name => "HC 2", :parent_id => od_1.id)
		user_3 = User.create(:name => 'User3 HC2', :password => '123', :password_confirmation => '123', :place_id => hc_2.id)

		report_1 = Report.create(:sender => "User1 HC 1", :disease => "ARI",:quantity => 5,
              :from => "sms://855246402340", :lat => "11.51030919", :lon => "105.05205696", :to => "sms://010232020", :user_id => user_1.id,:place_id => hc_1.id)
  	
		report_2 = Report.create(:sender => "User3 HC 2", :disease => "ARI",:quantity => 5,
              :from => "sms://855246402340", :lat => "11.51030919", :lon => "105.05205696", :to => "sms://010232020", :user_id => user_2.id,:place_id => hc_1.id)
		
		report_4 = Report.create(:sender => "User3 HC 2", :disease => "ARI",:quantity => 5,
              :from => "sms://855246402340", :lat => "11.51030919", :lon => "105.05205696", :to => "sms://010232020", :user_id => user_1.id,:place_id => hc_1.id)

		report_6 = Report.create(:sender => "User3 HC 2", :disease => "ARI",:quantity => 5,
              :from => "sms://855246402340", :lat => "11.51030919", :lon => "105.05205696", :to => "sms://010232020", :user_id => user_1.id,:place_id => hc_1.id)

		report_3 = Report.create(:sender => "User3 HC 2", :disease => "ARI",:quantity => 5,
              :from => "sms://855246402340", :lat => "11.51030919", :lon => "105.05205696", :to => "sms://010232020", :user_id => user_3.id,:place_id => hc_2.id)
		report_5 = Report.create(:sender => "User3 HC 2", :disease => "ARI",:quantity => 5,
              :from => "sms://855246402340", :lat => "11.51030919", :lon => "105.05205696", :to => "sms://010232020", :user_id => user_3.id,:place_id => hc_2.id)
		
  	end

  	it "should successful" do
		Old::Place.includes(:place_type).all.each do |place|
			Place.import_place place
		end

		p Report.count
		
  	end
  end


end


