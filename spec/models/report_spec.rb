require 'spec_helper'
describe Report do

  describe "handle_update_and_create" do
    before(:each) do
      @phd_1 = Phd.create(:name => "PHD 1")
      @od_1 = Od.create(:name => "OD 1", :parent_id => @phd_1.id)
      @hc_1 = Hc.create(:name => "HC 1", :parent_id => @od_1.id)
      @user = User.create(:name => "Kimhuot",:place_id => @hc_1.id, :phone_number => "85593608166",:password => '654321', :password_confirmation => '654321')
      Report.create(:sender => "Kimhuot", :disease => "AWD", :quantity => 10, :place_id => @hc_1.id)
    end

    describe "report exists" do
      it "should update" do
        # place_id = @hc_1.id
        diseases = {"AWD" => 25}
        params = {:sender=>"Kimhuot",:from => 'sms://85593608166', :lat => '48.74102610',
         :lon => '-117.41718860', :to => 'sms://2020'}
        Report.handle_update_and_create @hc_1.id,  diseases, params
        records = Report.all
        records[0].disease.should eq("AWD")
        records[0].quantity.should eq(25)
        records.count.should eq 1
      end
    end

    describe "report does not exist" do
      it "should create report" do
        diseases = {"AFP" => 0}
        params = {:sender=>"Kimhuot",:from => 'sms://85593608166', :lat => '48.74102610',
         :lon => '-117.41718860', :to => 'sms://2020'}
        Report.handle_update_and_create @hc_1.id, diseases, params
        records = Report.all
        records[0].disease.should eq("AWD")
        records[0].quantity.should eq(10)
        records[1].disease.should eq("AFP")
        records[1].quantity.should eq(0)
        records.count.should eq 2
      end
    end

    describe "report with No_Disease" do
      it "should update" do
        diseases = {"AWD" => 25, "No_Disease" => 0}
        params = {:sender=>"Kimhuot",:from => 'sms://85593608166', :lat => '48.74102610',
         :lon => '-117.41718860', :to => 'sms://2020'}
        Report.handle_update_and_create @hc_1.id, diseases, params
        records = Report.all
        records[0].disease.should eq("AWD")
        records[0].quantity.should eq(0)
        records[1].disease.should eq("No_Disease")
        records[1].quantity.should eq(0)
        records.count.should eq 2
      end
    end

  end

end