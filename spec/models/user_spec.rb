require 'spec_helper'

describe User do
  
  # describe "#create_new_user" do
  #   it "should create user if the user doesn't exist yet" do
  #     place = Phd.create(:name => 'phd_1')
  #     user = User.create(:name => 'Kimhuot',:place_id => place.id, :phone_number => "85593608166",:password => '123', :password_confirmation => '123')
  #     @users = User.all
  #     @users[0].name.should eq 'Kimhuot'
  #     @u.errors.full_messages.size.should eq 0
  #   end
  # end

  describe "#change_password" do
  	before(:each) do
		  @user = User.create(:name => 'keansvay', :place_id => 1, :password=> "123456", :password_confirmation => '123456')
  	end

    it "does not change password and return false if old password is incorrect" do
       
       result = @user.change_password(:old_password => '123', :password => '654321', :password_confirmation => '654321' )
       result.should eq false
       other_user = User.find(@user.id)
       other_user.password.should eq '123456'
       @user.errors.full_messages[0].should eq "Old password doesn't match"
    end

    it "does not change password when password doesn't match password_confirmation" do

       result = @user.change_password(:old_password => '123456', :password => '123', :password_confirmation => '654321' )
       result.should eq false
       other_user = User.find(@user.id)
       other_user.password.should eq '123456'
       @user.errors.full_messages[0]
    end

    it "change password to 123 and return true if old password is correct and password match password_confirmation" do
       result = @user.change_password(:old_password => '123456', :password => '123', :password_confirmation => '123' )
       result.should eq true
       other_user = User.find(@user.id)
       other_user.password.should eq '123'
       @user.errors.full_messages.size.should eq 0
    end

  end

  describe ".authenticate" do
    before(:each) do
      place = Phd.create(:name => 'phd_1')
      @user = User.create!(:name => 'test', :place_id => place.id, :password=> '123', :password_confirmation => '123')
    end
    context "with corrent username and password" do
      it "return user" do
        result = User.authenticate('test','123')
        result.name.should eq 'test'
      end
    end

    context "with incorrect username and password" do
      it "return nil" do
        result = User.authenticate('test','321')
        result.should eq false
      end
    end
  end


  describe ".construct_place" do

    before(:each)do
      @phd_1 = Phd.create(:name => "PHD 1")
      @phd_2 = Phd.create(:name => "PHD 2")

      @od_1 = Od.create(:name => "OD 1", :parent_id => @phd_1.id)
      @od_2 = Od.create(:name => "OD 2", :parent_id => @phd_1.id)

      @hc_1 = Hc.create(:name => "HC 1", :parent_id => @od_1.id)
      @hc_2 = Hc.create(:name => "HC 2", :parent_id => @od_1.id)

      @hc_3 = Hc.create(:name => "HC 3", :parent_id => @od_2.id)

      @od_3 = Od.create(:name => "OD 3", :parent_id => @phd_2.id)
      @hc_3 = Hc.create(:name => "HC 3", :parent_id => @od_3.id)
    end

    it "return option_place" do
        result_opt = []
        User.construct_place @phd_1, result_opt, 1
        result_opt.count.should eq 6
        # result_opt[0] 
    end
  end


  describe "members" do
    
    before(:each) do
      @phd_1 = Phd.create(:name => "PHD 1")
      @phd_2 = Phd.create(:name => "PHD 2")

      @od_1 = Od.create(:name => "OD 1", :parent_id => @phd_1.id)
      @od_2 = Od.create(:name => "OD 2", :parent_id => @phd_1.id)
      @od_3 = Od.create(:name => "OD 2", :parent_id => @phd_2.id)

      @hc_1 = Hc.create(:name => "HC 1", :parent_id => @od_1.id)
      @hc_2 = Hc.create(:name => "HC 2", :parent_id => @od_1.id)

      @hc_3 = Hc.create(:name => "HC 3", :parent_id => @od_2.id)

      @user_1 = User.create(:name => "Kimhuot",:place_id => @phd_1.id, :phone_number => "85593608166",:password => '654321', :password_confirmation => '654321')
      @user_2 = User.create(:name => "test",:place_id => @od_2.id, :phone_number => "85512234567",:password => '123', :password_confirmation => '123')
      @user_3 = User.create(:name => "user1",:place_id => @od_1.id, :phone_number => "85512234568",:password => '123', :password_confirmation => '123')
      @user_4 = User.create(:name => "user1",:place_id => @phd_2.id, :phone_number => "85512234569",:password => '123', :password_confirmation => '123')
    
    end

    it "return member by parent place" do
      result = @user_1.members
      result.count.should eq 2

      result = @user_4.members
      result.count.should eq 0

    end
  end


  describe "create user" do
    context "with user level 1" do
      it "should create user successfully" do
        @user_admin = User.new(:name => 'admin', :password => '123', :password_confirmation => '123', :can_login => true, :level => 1)
        result = @user_admin.save
        expect(result).to be_true
      end
    end
  end

end
